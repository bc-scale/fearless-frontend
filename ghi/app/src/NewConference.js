import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {      
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            locations: [],
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartChange = this.handleStartChange.bind(this);
        this.handleEndChange = this.handleEndChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this) ;

    };

    async handleSubmit(event) {

      event.preventDefault();
      const data = {...this.state};
      // data is equal to state of the infro I inputted
      // var(this.state) that has info that I want to track
      // state change casues re-render
      console.log(data);
      delete data.locations
      
      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

          const cleared = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            location: '',
          };
          this.setState(cleared);
        }
    }

    handleNameChange(event) {
      const value = event.target.value;
      this.setState({name: value})
    }

    handleStartChange(event) {
        const value = event.target.value;
        this.setState({starts: value})
    }
    
    handleEndChange(event) {
        const value = event.target.value;
        this.setState({ends: value})
    }

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value})
    }

    handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({max_presentations: value})
    }

    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({max_attendees: value})
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }

        
    async componentDidMount(props) {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
    
        if (response.ok) {
          const data = await response.json();
          this.setState({locations:data.locations});

        }
      }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
                  {/* add submit  */}
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStartChange} placeholder="Start-date" required type="date" name="starts" id="start-date" className="form-control" />
                <label htmlFor="start-date">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleEndChange} placeholder="End-date" required type="date" name="ends" id="end-date" className="form-control" />
                <label htmlFor="end-date">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <textarea onChange={this.handleDescriptionChange}  placeholder="description" required type="textaera" name="description" id="description" className="form-control" cols="30" rows="40"></textarea>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxPresentationsChange} placeholder="Maximum-presentation" required type="number" name="max_presentations" id="maximum-presentation" className="form-control" min="0" />
                <label htmlFor="maximum-presentation">Maximum presentation</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxAttendeesChange} placeholder="Maximum-attndees" required type="number" name="max_attendees" id="maximum-attndees" className="form-control" min="0" />
                <label htmlFor="maximum-attndees">Maximum attndees</label>
              </div>
              <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                { this.state.locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>{location.name}</option>
                        )
                    })}

              </select>
              <br/>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      );
  }
}

export default ConferenceForm;
