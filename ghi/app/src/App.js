import './App.css';
import { BrowserRouter, Route, Routes, } from "react-router-dom";

import Nav from './Nav';

import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './NewConference';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="contianer">
      <Routes>

      <Route index element={<MainPage />} />


        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>

        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
        </Route>

        <Route path="attendees">
          <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
        </Route>
        
        <Route path="attendees">
          <Route path="new" element={<AttendConferenceForm />} />
        </Route>

        <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>

      </Routes>
      </div>
    </BrowserRouter>

  );
}

export default App;
